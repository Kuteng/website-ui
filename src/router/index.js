import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import edit from '@/components/idea/edit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/idea/edit',
      name: 'Idea编辑',
      component: edit
    },
    {
      path: '/login',
      meta: {
        title: '登录界面1'
      },
      component: (resolve) => require(['@/components/manage/login'], resolve)
    },
    {
      path: '/idea/list',
      meta: {
        title: 'Idea列表'
      },
      component: (resolve) => require(['@/components/idea/list'], resolve)
    },
    {
      path: '/image/view',
      meta: {
        title: '套图浏览'
      },
      component: (resolve) => require(['@/components/image/view'], resolve)
    }
  ]
})
