# website-ui

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# run eslint
npm run lint

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## 涉及的组件

- vue-router
- iview: `npm install iview --save`
- axios: `npm install axios --save`
- vue-axios: `npm install vue-axios --save`

## 其他文件
[EsLint规则详解](docs/eslint_roles.md)
